<?php

if($_POST) {

// ------------------
// Check for AJAX request
// ------------------

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {

    $feedback = json_encode(array(
        'type'=>'error',
        'text' => 'Request type must be AJAX POST'
    ));

    die($feedback);
}

// ------------------
// Fetch Form Content
// ------------------

// Error Message

function died($error) {

    echo "We are very sorry, but there were error(s) found with the form you submitted. ";
    echo "These errors appear below.<br /><br />";
    echo $error."<br /><br />";
    echo "Please go back and fix these errors.<br /><br />";

    die();

}

// Date

$date           = date("d.m.o");

// Form Content

$reference      = $_POST['reference'];
$salutation     = $_POST['salutation']; // required
$firstname      = $_POST['firstname']; // required
$lastname       = $_POST['lastname']; // required
$company        = $_POST['company'];
$street         = $_POST['street']; // required
$zip            = $_POST['zip']; // required
$city           = $_POST['city']; // required
$phone          = $_POST['phone']; // required
$email          = $_POST['email']; // required
$message        = $_POST['message']; // required

$name           = $firstname .' '. $lastname;


// Check for Reference Number

if ( !empty($reference) ) {
    $reference = '['. $reference .'] ';
}

// Check for Company

if ( !empty($company) ) {
    $company = '<tr><td style="color: #999999; font-size: 12px; text-transform: uppercase; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Firma</td><td style="color: #2b3133; font-size: 14px; padding-left: 16px; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">'. $company .'</td></tr>';
}

// ------------------
// E-Mail Basics
// ------------------

// Recipient

$emailRecipient = 'mail@ra-nolte.net';

// Subject

$emailSubject = '[ra-nolte.net] '. $reference .'Nachricht über Kontakformular';

// ------------------
// E-Mail Message
// ------------------

$emailMessage = '
<html style="margin: 0;">
<body style="margin: 0;">
<table bgcolor="#f2f2f2" cellpadding="0" cellspacing="0">
<tr>
<td>
<div class="card-wrapper" style="background-color: #f2f2f2; font-family: Helvetica, sans-serif; padding: 10px;">
<div class="card" style="background-color: #fff; border-bottom: 1px solid rgba(0, 0, 0, 0.15); border-left: 1px solid rgba(0, 0, 0, 0.07); border-right: 1px solid rgba(0, 0, 0, 0.07); box-shadow: 0 2px 4px rgba(0,0,0,.1); margin: 0 auto 64px; max-width: 500px; padding: 20px 20px 60px">
<div class="card-header" style="margin-bottom: 64px; text-align: center;">
<img src="http://ra-nolte.net/assets/images/logo-rechtsanwaeltin-nolte.png" alt="Astrid Nolte" height="130" width="123">
</div>
<div class="card-body">
<div style="margin-bottom: 32px;">
<h1 style="color: #444444; font-size: 18px; margin: 0; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">'. $reference .'Nachricht vom '. $date .'</h1>
</div>
<div style="border-bottom: 1px solid #cccccc; margin-bottom: 32px; padding-bottom: 32px;">
<h2 style="color: #999999; font-size: 14px; margin: 0; text-transform: uppercase; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Nachricht</h2>
<p style="color: #2b3133; font-size: 14px; line-height: 18px; margin: 8px 0 24px; white-space: pre-wrap; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">'. $message .'</p>
<a href="mailto:'. $email .'?subject=Re: '. $reference .'Ihre Nachricht vom '. $date .'" style="background-color: #00B2FF; background-image: radial-gradient(circle, rgba(0, 178, 255, 0), rgba(0, 178, 255, 0.8)); border: 0; border-radius: 3px; color: white; cursor: pointer; display: inline-block; font-size: 16px; font-weight: 700; padding: 12px 14px 10px; text-align: center; text-shadow: 0 1px 0 hsla(0,0%,0%,0); text-transform: uppercase; transition: all 150ms ease-out; text-decoration: none; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Sofort antworten</a>
</div>
<div>
<h2 style="color: #999999; font-size: 14px; margin: 0 0 16px; text-transform: uppercase; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Kontaktdaten</h2>
<table cellpadding="0" cellspacing="0">
<tr>
<td></td>
<td style="color: #2b3133; font-size: 14px; padding-left: 16px; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">'. $formSalutation .'</td>
</tr>
<tr>
<td style="color: #999999; font-size: 12px; text-transform: uppercase; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Name</td>
<td style="color: #2b3133; font-size: 14px; padding-left: 16px; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">'. $firstname .' '. $lastname .'</td>
</tr>
'. $company .'
<tr>
<td style="color: #999999; font-size: 12px; text-transform: uppercase; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Anschrift</td>
<td style="color: #2b3133; font-size: 14px; padding-left: 16px; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">'. $street .'</td>
</tr>
<tr>
<td style="padding-bottom: 8px;"></td>
<td style="padding-bottom: 8px; color: #2b3133; font-size: 14px; padding-left: 16px; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">'. $zip .' '. $city .'</td>
</tr>
<tr>
<td style="color: #999999; font-size: 12px; text-transform: uppercase; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Telefon</td>
<td style="color: #2b3133; font-size: 14px; padding-left: 16px; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">'. $phone .'</td>
</tr>
<tr>
<td style="color: #999999; font-size: 12px; text-transform: uppercase; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">E-Mail</td>
<td style="color: #2b3133; font-size: 14px; padding-left: 16px; vertical-align: bottom; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;"><a href="mailto:'. $email .'?subject=Re: '. $reference .'Ihre Nachricht vom '. $date .'" style="color: #00B2FF; text-decoration: none;">'. $email .'</a></td>
</tr>
</table>
</div>
</div>
</div>
<div class="footer">
    <p style="color: #999999; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;"><small style="font-size: 11px;">Diese E-Mail enth&auml;lt vertrauliche und/oder rechtlich gesch&uuml;tzte Informationen. Wenn Sie nicht der richtige Adressat sind oder diese E-Mail irrt&uuml;mlich erhalten haben, informieren Sie bitte sofort Rechtsanw&auml;ltin Astrid Nolte per E-Mail unter <a href="mailto:mail@ra-nolte.net" style="color: #00B2FF; text-decoration: none;">mail@ra-nolte.net</a> und l&ouml;schen Sie diese E-Mail. Das unerlaubte Kopieren sowie die unbefugte Weitergabe dieser E-Mail und der darin enthaltenen Informationen ist nicht gestattet. Vielen Dank.</small></p>
    <p style="color: #999999; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;"><small style="font-size: 11px;">This e-mail may contain confidential and/or privileged information. If you are not the intended recipient (or have received this email in error) please immediately notify Astrid Nolte at <a href="mailto:mail@ra-nolte.net" style="color: #00B2FF; text-decoration: none;">mail@ra-nolte.net</a> and delete this e-mail. Any unauthorized copying, disclosure or distribution of the material in this e-mail is strictly forbidden. Thank you for your cooperation.</small></p>
</div>
</div>
</td>
</tr>
</table>
</body>
</html>
';

// ------------------
// E-Mail Headers
// ------------------

$emailHeaders   = array();

// HTML

$emailHeaders[] = "MIME-Version: 1.0";
$emailHeaders[] = "Content-type: text/html; charset=iso-8859-1";

// Additional

$emailHeaders[] = "From: {$name} <mail@ra-nolte.net>";
$emailHeaders[] = "Reply-To: {$email}";
$emailHeaders[] = "Subject: {$emailSubject}";
$emailHeaders[] = "X-Mailer: PHP/".phpversion();

// ------------------
// Send E-Mail
// ------------------

$sendMail = mail($emailRecipient, $emailSubject, $emailMessage, implode("\r\n", $emailHeaders));

if ( !$sendMail ) {
    $feedback = json_encode(array('type'=>'error'));
    die($feedback);
} else {
    $feedback = json_encode(array('type'=>'message'));
    die($feedback);
}

}

?>
