$('.popup').fancybox({
    beforeShow: function() {
        $('.fancybox-inner').css('height','auto');
        $('body').addClass('print-detail');
    },
    beforeClose: function() {
        $('body').removeClass('print-detail');
    },
    helpers : {
        title: {
            type: 'inside',
            position: 'top'
        }
    },
    margin: [52, 10, 10, 10],
    maxWidth: 700,
    padding: 0,
    scrolling: 'visible',
    wrapCSS: 'fancybox-popup',
    parent: '.details-container'
});

// Fixed Header

var headhesive = new Headhesive('.main-header', {
    offset: 490,
    throttle: 150
});

// Scroll to Sections

$('.main-navigation a').on('click', function(event) {
    'use strict';
    event.preventDefault();

    var animateScrollPadding = 0;

    if ($(window).width() >= 640) {
        animateScrollPadding = 56;
    }

    var target = $(this).attr('href');

    $(target).animatescroll({
        scrollSpeed: 800,
        easing: 'easeInOutCirc',
        padding: animateScrollPadding
    });
})

// Contact Form

$('.contact-form').validetta({
    display: 'inline',
    onValid: function(event) {
        event.preventDefault();
        $.ajax({
            url: 'form-contact.php',
            data: $('.contact-form').serialize(),
            dataType: 'json',
            type: 'POST',
            beforeSend: function(){
                console.log("Head 'em up, move 'em out… Power stride, and ready to ride.");
            }
        })
        .done( function(data) {
            $.fancybox.close();
            $.fancybox('<div class="grid-container"> <div class="grid-row"> <div class="grid-col-1" style="margin-bottom: 0;"><p>Vielen Dank für Ihre Anfrage. Wir werden uns um Ihr Anliegen kümmern und uns schnellstmöglichst bei Ihnen melden.</p></div></div></div>', {
                beforeShow: function() {
                    $('.fancybox-inner').css('height','auto');
                },
                helpers : {
                    title : {
                        type: 'inside',
                        position: 'top'
                    }
                },
                beforeLoad : function() {
                    this.title = 'E-Mail';
                },
                margin: [52, 10, 10, 10],
                maxWidth: 700,
                padding: 0,
                scrolling: 'visible',
                wrapCSS: 'fancybox-popup'
            });
        })
        .fail( function(data) {
            console.log('nope');
        });
    }
});

// Google Maps

// var mapOptions = {
//     center: { lat: 51.259377, lng: 6.378527},
//     zoom: 16,
//     disableDefaultUI: true,
//     scrollwheel: false,
//     disableDoubleClickZoom: true,
//     zoomControl: false
//
// };

function initialize() {
    var mapCenter      = { lat: 51.259377, lng: 6.378527};

    if ($(window).width() >= 640 ) {
        mapCenter = { lat: 51.259377, lng: 6.386127};
    } else {
        mapCenter = { lat: 51.256, lng: 6.381};
    } //else {
        //mapCenter = { lat: 51.256000, lng: 6.381000};
    //}

    var mapOptions = {
        center: mapCenter,
        zoom: 16,
        disableDefaultUI: true,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        zoomControl: false,
        draggable: false
    };

    var map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);

    var url = 'assets/images/map-marker.png';
    var size = new google.maps.Size(34, 49);

    if(window.devicePixelRatio >= 1.5){
        url = 'assets/images/map-marker@2x.png';
        size = new google.maps.Size(68, 98);
    }

    var image = {
        url: url,
        size: size,
        scaledSize: new google.maps.Size(34, 49),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(16, 40)
    };
    var myLatLng = new google.maps.LatLng(51.2581002, 6.3809233);
    var beachMarker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image,
        title: 'Dülkener Straße 71'
    });
}

google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, 'resize', initialize);
