'use strict';

var gulp        = require('gulp'),
    $           = require('gulp-load-plugins')(),
    connect     = require('gulp-connect'),
    pngquant    = require('imagemin-pngquant');

var AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

gulp.task('default', function() {
    gulp.start(['build']);
});

// Image Task
// Optimize PNG, JPG and SVG
gulp.task('images', function () {
    gulp.src('src/assets/images/*')
    .pipe($.imagemin({
        interlaced: true,
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
    }))
    .pipe(gulp.dest('dist/assets/images'))
    .pipe($.size())
    .pipe(connect.reload());
});

// Styles Task
// Autoprefix and compile SASS

gulp.task('styles', function() {
  return gulp.src('src/assets/styles/*.scss')
    //.pipe(plugins.sourcemaps.init())
      .pipe($.sass({errLogToConsole: true}).on('error', $.sass.logError))
      .pipe($.autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
      }))
      .pipe($.cssmin({
        keepSpecialComments: true
      }))
    //.pipe(plugins.sourcemaps.write())
    .pipe(gulp.dest('dist/assets/styles'))
});

// Scripts Task
// Uglify
gulp.task('scripts', function() {
    gulp.src('src/assets/scripts/main.js')
    .pipe($.uglify())
    .pipe(gulp.dest('dist/assets/scripts'))
    .pipe($.size())
    .pipe(connect.reload());

    gulp.src('src/assets/scripts/vendor/*.js')
    .pipe($.uglify())
    .pipe($.concat('vendor.js'))
    .pipe(gulp.dest('dist/assets/scripts'))
    .pipe($.size())
    .pipe(connect.reload());
});

// HTML Tasks
// Copy theme files to dist
gulp.task('html', function() {
    gulp.src('src/*.html')
    .pipe($.htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'))
    .pipe($.size())
    .pipe(connect.reload());
});

// Connect Task
// Starts a server and comes with Live Reload
gulp.task('connect', function() {
    connect.server({
        root: 'dist',
        livereload: true
    });
});

// Build Task
gulp.task('build', ['styles', 'scripts', 'html', 'images']);

// Watch Task
// Watches src for file changes/additions/removals
gulp.task('watch', ['connect'], function () {
    gulp.watch('src/assets/images/*.*', ['images']);
    gulp.watch('src/assets/styles/**/*.*', ['styles']);
    gulp.watch('src/assets/scripts/**/*.*', ['scripts']);
    gulp.watch('src/*.html', ['html']);
    gulp.watch('bower.json', ['wiredep']);
});
